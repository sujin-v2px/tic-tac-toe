package tictactoe;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

/**
 *
 * @author acer
 */
public class TicTacToe extends JFrame {

    private final int CELL_COUNT = 9;
    private JButton[] grid = new JButton[CELL_COUNT];

    private final String EMPTY = "";
    private final String X = "X";
    private final String O = "O";

    private String currentPlayer = X;

    public TicTacToe() {
        setTitle("Tic Tac Toe");
        setLayout(new GridLayout(3, 3));
        createGrid();
        pack();
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setResizable(false);
        setLocation(screenCenter());
    }

    private void createGrid() {
        for (int i = 0; i < grid.length; i++) {
            grid[i] = createCell();
            add(grid[i]);
        }
    }

    private JButton createCell() {
        JButton b = new JButton(EMPTY);
        b.setFont(b.getFont().deriveFont(50.0f));
        b.setPreferredSize(new Dimension(100, 100));
        b.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                onClick(b);

            }
        });
        return b;
    }

    private void showDialog() {
        int result = JOptionPane.showConfirmDialog(null, "Player " + currentPlayer + " Won" + "\nDo you want to play again?", null, JOptionPane.YES_NO_OPTION);
        if (result == JOptionPane.OK_OPTION) {
            for (JButton b : grid) {
                b.setText(EMPTY);
            }
        } else {
            JOptionPane.showMessageDialog(null, "Goodbye");
            System.exit(0);
        }
    }

    private void onClick(JButton b) {
        if (!b.getText().equals(EMPTY)) {
            return;
        }
        b.setText(currentPlayer);
        if (hasWon()) {
            showDialog();
        }
        changeCurrentPlayer();

    }

    public boolean hasWon() {

        return    (((grid[0].getText().equals(currentPlayer)) && (grid[1].getText().equals(currentPlayer)) && (grid[2].getText().equals(currentPlayer)))
                || ((grid[3].getText().equals(currentPlayer)) && (grid[4].getText().equals(currentPlayer)) && (grid[5].getText().equals(currentPlayer)))
                || ((grid[6].getText().equals(currentPlayer)) && (grid[7].getText().equals(currentPlayer)) && (grid[8].getText().equals(currentPlayer)))
                || ((grid[0].getText().equals(currentPlayer)) && (grid[3].getText().equals(currentPlayer)) && (grid[6].getText().equals(currentPlayer)))
                || ((grid[1].getText().equals(currentPlayer)) && (grid[4].getText().equals(currentPlayer)) && (grid[7].getText().equals(currentPlayer)))
                || ((grid[2].getText().equals(currentPlayer)) && (grid[5].getText().equals(currentPlayer)) && (grid[8].getText().equals(currentPlayer)))
                || ((grid[0].getText().equals(currentPlayer)) && (grid[4].getText().equals(currentPlayer)) && (grid[8].getText().equals(currentPlayer)))
                || ((grid[2].getText().equals(currentPlayer)) && (grid[4].getText().equals(currentPlayer)) && (grid[6].getText().equals(currentPlayer))));

    }

    private void changeCurrentPlayer() {
        currentPlayer = currentPlayer.equals(X) ? O : X;
    }

    private Point screenCenter() {
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        int sw = screenSize.width, sh = screenSize.height;
        int w = getSize().width, h = getSize().height;
        return new Point((sw - w) / 2, (sh - h) / 2);
    }

    
    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new TicTacToe().setVisible(true);
            }
        });
    }

}
